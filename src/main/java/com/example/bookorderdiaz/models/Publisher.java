package com.example.bookorderdiaz.models;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "publishers")
public class Publisher extends AuditModel {
    @Id
    @Column(name = "publisher_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long publisherId;

    @Column(name = "publisher_company_name")
    private String companyName;

    @Column(name = "publisher_country")
    private String publisherCountry;

    @ManyToOne
    @JoinColumn(name = "paper_id")
    private Paper paper;

    @OneToMany(mappedBy = "publisher")
    private List<Book> books;

    public Publisher() {
    }

    public Publisher(String companyName, String publisherCountry) {
        this.companyName = companyName;
        this.publisherCountry = publisherCountry;
    }

    public Long getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Long publisherId) {
        this.publisherId = publisherId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPublisherCountry() {
        return publisherCountry;
    }

    public void setPublisherCountry(String publisherCountry) {
        this.publisherCountry = publisherCountry;
    }

    public Paper getPaper() {
        return paper;
    }

    public void setPaper(Paper paper) {
        this.paper = paper;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
}
