package com.example.bookorderdiaz.models;

import javax.persistence.*;

@Entity
@Table(name = "book_ratings")
public class BookRating extends AuditModel {
    @Id
    @Column(name = "book_rating_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long bookRatingId;

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book;

    @ManyToOne
    @JoinColumn(name = "reviewer_id")
    private Reviewer reviewer;

    @Column(name = "book_rating_score")
    private Integer ratingScore;

    public BookRating() {
    }

    public BookRating(Integer ratingScore) {
        this.ratingScore = ratingScore;
    }

    public Long getBookRatingId() {
        return bookRatingId;
    }

    public void setBookRatingId(Long bookRatingId) {
        this.bookRatingId = bookRatingId;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Reviewer getReviewer() {
        return reviewer;
    }

    public void setReviewer(Reviewer reviewer) {
        this.reviewer = reviewer;
    }

    public Integer getRatingScore() {
        return ratingScore;
    }

    public void setRatingScore(Integer ratingScore) {
        this.ratingScore = ratingScore;
    }
}
