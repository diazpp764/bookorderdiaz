package com.example.bookorderdiaz.models;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "orders")
public class Order extends AuditModel {
    @Id
    @Column(name = "order_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderId;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @Column(name = "order_date")
    private Date orderDate;

    @Column(name = "total_order")
    private BigDecimal totalOrder;

    @OneToMany(mappedBy = "order")
    private List<OrderBookDetails> listOrderBookDetails;

    public Order() {
    }

    public Order(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public BigDecimal getTotalOrder() {
        return totalOrder;
    }

    public void setTotalOrder(BigDecimal totalOrder) {
        this.totalOrder = totalOrder;
    }

    public List<OrderBookDetails> getListOrderBookDetails() {
        return listOrderBookDetails;
    }

    public void setListOrderBookDetails(List<OrderBookDetails> listOrderBookDetails) {
        this.listOrderBookDetails = listOrderBookDetails;
    }
}
