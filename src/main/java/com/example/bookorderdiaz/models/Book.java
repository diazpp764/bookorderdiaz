package com.example.bookorderdiaz.models;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "books")
public class Book extends AuditModel {
    @Id
    @Column(name = "book_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long bookId;

    @Column(name = "book_title")
    private String title;

    @Column(name = "book_released_date")
    private Date releasedDate;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private Author author;

    @ManyToOne
    @JoinColumn(name = "publisher_id")
    private Publisher publisher;

    @Column(name = "book_price")
    private BigDecimal bookPrice;

    @OneToMany(mappedBy = "book")
    private List<BookRating> bookRatings;

    @OneToMany(mappedBy = "book")
    private List<OrderBookDetails> orderBookDetails;

    public Book() {
    }

    public Book(String title, Date releasedDate) {
        this.title = title;
        this.releasedDate = releasedDate;
        calculateBookPrice();
    }

    public void calculateBookPrice() {
        BigDecimal result, pricePercentage = new BigDecimal(1.5);

        result = this.publisher.getPaper().getPaperPrice().multiply(pricePercentage);

        setBookPrice(result);
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getReleasedDate() {
        return releasedDate;
    }

    public void setReleasedDate(Date releasedDate) {
        this.releasedDate = releasedDate;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public BigDecimal getBookPrice() {
        return bookPrice;
    }

    public void setBookPrice(BigDecimal bookPrice) {
        this.bookPrice = bookPrice;
    }

    public List<BookRating> getBookRatings() {
        return bookRatings;
    }

    public void setBookRatings(List<BookRating> bookRatings) {
        this.bookRatings = bookRatings;
    }

    public List<OrderBookDetails> getOrderBookDetails() {
        return orderBookDetails;
    }

    public void setOrderBookDetails(List<OrderBookDetails> orderBookDetails) {
        this.orderBookDetails = orderBookDetails;
    }
}
