package com.example.bookorderdiaz.models;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "papers")
public class Paper extends AuditModel {
    @Id
    @Column(name = "paper_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long paperId;

    @Column(name = "paper_quality_name")
    private String qualityName;

    @Column(name = "paper_price")
    private BigDecimal paperPrice;

    @OneToMany(mappedBy = "paper")
    private List<Publisher> publishers;

    public Paper() {
    }

    public Paper(String qualityName, BigDecimal paperPrice) {
        this.qualityName = qualityName;
        this.paperPrice = paperPrice;
    }

    public Long getPaperId() {
        return paperId;
    }

    public void setPaperId(Long paperId) {
        this.paperId = paperId;
    }

    public String getQualityName() {
        return qualityName;
    }

    public void setQualityName(String qualityName) {
        this.qualityName = qualityName;
    }

    public BigDecimal getPaperPrice() {
        return paperPrice;
    }

    public void setPaperPrice(BigDecimal paperPrice) {
        this.paperPrice = paperPrice;
    }

    public List<Publisher> getPublishers() {
        return publishers;
    }

    public void setPublishers(List<Publisher> publishers) {
        this.publishers = publishers;
    }
}
