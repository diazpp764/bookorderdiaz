package com.example.bookorderdiaz.models;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "reviewers")
public class Reviewer extends AuditModel {
    @Id
    @Column(name = "reviewer_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long reviewerId;

    @Column(name = "reviewer_name")
    private String reviewerName;

    @Column(name = "reviewer_country")
    private String reviewerCountry;

    @Column(name = "verified_status")
    private Boolean verifiedStatus;

    @OneToMany(mappedBy = "reviewer")
    private List<BookRating> bookRatings;

    public Reviewer() {
    }

    public Reviewer(String reviewerName, String reviewerCountry, Boolean verifiedStatus) {
        this.reviewerName = reviewerName;
        this.reviewerCountry = reviewerCountry;
        this.verifiedStatus = verifiedStatus;
    }

    public Long getReviewerId() {
        return reviewerId;
    }

    public void setReviewerId(Long reviewerId) {
        this.reviewerId = reviewerId;
    }

    public String getReviewerName() {
        return reviewerName;
    }

    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

    public String getReviewerCountry() {
        return reviewerCountry;
    }

    public void setReviewerCountry(String reviewerCountry) {
        this.reviewerCountry = reviewerCountry;
    }

    public Boolean getVerifiedStatus() {
        return verifiedStatus;
    }

    public void setVerifiedStatus(Boolean verifiedStatus) {
        this.verifiedStatus = verifiedStatus;
    }

    public List<BookRating> getBookRatings() {
        return bookRatings;
    }

    public void setBookRatings(List<BookRating> bookRatings) {
        this.bookRatings = bookRatings;
    }
}
