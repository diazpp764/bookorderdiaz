package com.example.bookorderdiaz.models;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class OrderBookDetails extends AuditModel {
    @EmbeddedId
    private OrderDetailsKey orderDetailsId;

    @ManyToOne
    @MapsId("order_id")
    @JoinColumn(name = "order_id")
    private Order order;

    @ManyToOne
    @MapsId("book_id")
    @JoinColumn(name = "book_id")
    private Book book;

    @Column(name = "order_quantity")
    private Integer quantity;

    @Column(name = "order_discount")
    private BigDecimal discount;

    @Column(name = "order_tax")
    private BigDecimal tax;

    public OrderBookDetails() {
    }

    public OrderBookDetails(Integer quantity, BigDecimal discount) {
        this.quantity = quantity;
        this.discount = discount;
        calculateTax();
    }

    public void calculateTax() {
        BigDecimal totalTax, bookPrice, bookQuantity;
        bookPrice = this.book.getBookPrice();
        bookQuantity = BigDecimal.valueOf(this.quantity);

        totalTax = (bookQuantity.multiply(bookPrice)).multiply(BigDecimal.valueOf(0.05));

        setTax(totalTax);
    }

    public OrderDetailsKey getOrderDetailsId() {
        return orderDetailsId;
    }

    public void setOrderDetailsId(OrderDetailsKey orderDetailsId) {
        this.orderDetailsId = orderDetailsId;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }
}
