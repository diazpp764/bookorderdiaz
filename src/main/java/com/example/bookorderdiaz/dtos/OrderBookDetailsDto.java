package com.example.bookorderdiaz.dtos;

import com.example.bookorderdiaz.models.OrderDetailsKey;

import java.math.BigDecimal;

public class OrderBookDetailsDto {
    private OrderDetailsKey orderDetailsId;
    private OrderDto order;
    private BookDto book;
    private Integer quantity;
    private BigDecimal discount;
    private BigDecimal tax;

    public OrderBookDetailsDto() {
    }

    public OrderBookDetailsDto(Integer quantity, BigDecimal discount, BigDecimal tax) {
        this.quantity = quantity;
        this.discount = discount;
        this.tax = tax;
    }

    public OrderDetailsKey getOrderDetailsId() {
        return orderDetailsId;
    }

    public void setOrderDetailsId(OrderDetailsKey orderDetailsId) {
        this.orderDetailsId = orderDetailsId;
    }

    public OrderDto getOrder() {
        return order;
    }

    public void setOrder(OrderDto order) {
        this.order = order;
    }

    public BookDto getBook() {
        return book;
    }

    public void setBook(BookDto book) {
        this.book = book;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }
}
