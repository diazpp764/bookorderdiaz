package com.example.bookorderdiaz.dtos;

public class PublisherDto {
    private Long publisherId;
    private String companyName;
    private String publisherCountry;
    private PaperDto paper;

    public PublisherDto() {
    }

    public PublisherDto(Long publisherId, String companyName, String publisherCountry, PaperDto paper) {
        this.publisherId = publisherId;
        this.companyName = companyName;
        this.publisherCountry = publisherCountry;
        this.paper = paper;
    }

    public Long getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Long publisherId) {
        this.publisherId = publisherId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPublisherCountry() {
        return publisherCountry;
    }

    public void setPublisherCountry(String publisherCountry) {
        this.publisherCountry = publisherCountry;
    }

    public PaperDto getPaper() {
        return paper;
    }

    public void setPaper(PaperDto paper) {
        this.paper = paper;
    }
}
