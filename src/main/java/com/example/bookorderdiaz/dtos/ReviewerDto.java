package com.example.bookorderdiaz.dtos;

public class ReviewerDto {
    private Long reviewerId;
    private String reviewerName;
    private String reviewerCountry;
    private Boolean verifiedStatus;

    public ReviewerDto() {
    }

    public ReviewerDto(String reviewerName, String reviewerCountry, Boolean verifiedStatus) {
        this.reviewerName = reviewerName;
        this.reviewerCountry = reviewerCountry;
        this.verifiedStatus = verifiedStatus;
    }

    public Long getReviewerId() {
        return reviewerId;
    }

    public void setReviewerId(Long reviewerId) {
        this.reviewerId = reviewerId;
    }

    public String getReviewerName() {
        return reviewerName;
    }

    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

    public String getReviewerCountry() {
        return reviewerCountry;
    }

    public void setReviewerCountry(String reviewerCountry) {
        this.reviewerCountry = reviewerCountry;
    }

    public Boolean getVerifiedStatus() {
        return verifiedStatus;
    }

    public void setVerifiedStatus(Boolean verifiedStatus) {
        this.verifiedStatus = verifiedStatus;
    }
}
