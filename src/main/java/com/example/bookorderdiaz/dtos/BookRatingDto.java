package com.example.bookorderdiaz.dtos;

public class BookRatingDto {
    private Long bookRatingId;
    private BookDto book;
    private ReviewerDto reviewer;
    private Integer ratingScore;

    public BookRatingDto() {
    }

    public BookRatingDto(Integer ratingScore) {
        this.ratingScore = ratingScore;
    }

    public Long getBookRatingId() {
        return bookRatingId;
    }

    public void setBookRatingId(Long bookRatingId) {
        this.bookRatingId = bookRatingId;
    }

    public BookDto getBook() {
        return book;
    }

    public void setBook(BookDto book) {
        this.book = book;
    }

    public ReviewerDto getReviewer() {
        return reviewer;
    }

    public void setReviewer(ReviewerDto reviewer) {
        this.reviewer = reviewer;
    }

    public Integer getRatingScore() {
        return ratingScore;
    }

    public void setRatingScore(Integer ratingScore) {
        this.ratingScore = ratingScore;
    }
}
