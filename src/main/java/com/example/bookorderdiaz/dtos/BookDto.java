package com.example.bookorderdiaz.dtos;

import java.math.BigDecimal;
import java.util.Date;

public class BookDto {
    private Long bookId;
    private String title;
    private Date releasedDate;
    private AuthorDto author;
    private PublisherDto publisher;
    private BigDecimal bookPrice;

    public BookDto() {
    }

    public BookDto(String title, Date releasedDate, BigDecimal bookPrice) {
        this.title = title;
        this.releasedDate = releasedDate;
        this.bookPrice = bookPrice;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getReleasedDate() {
        return releasedDate;
    }

    public void setReleasedDate(Date releasedDate) {
        this.releasedDate = releasedDate;
    }

    public AuthorDto getAuthor() {
        return author;
    }

    public void setAuthor(AuthorDto author) {
        this.author = author;
    }

    public PublisherDto getPublisher() {
        return publisher;
    }

    public void setPublisher(PublisherDto publisher) {
        this.publisher = publisher;
    }

    public BigDecimal getBookPrice() {
        return bookPrice;
    }

    public void setBookPrice(BigDecimal bookPrice) {
        this.bookPrice = bookPrice;
    }
}
