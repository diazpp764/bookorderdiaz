package com.example.bookorderdiaz.controllers;

import com.example.bookorderdiaz.dtos.AuthorDto;
import com.example.bookorderdiaz.exceptions.ResourceNotFoundException;
import com.example.bookorderdiaz.models.Author;
import com.example.bookorderdiaz.repositories.AuthorRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class AuthorController {
    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private AuthorRepository authorRepository;

    // Get all author
    @GetMapping("/authors")
    public ResponseEntity<Object> getAllAuthors() {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";
        List<Author> listAllAuthorEntity = authorRepository.findAll();
        List<AuthorDto> listAllAuthorDto = new ArrayList<>();

        if (listAllAuthorEntity.size() >= 1) {
            for (Author author : listAllAuthorEntity) {
                AuthorDto authorDto = modelMapper.map(author, AuthorDto.class);

                listAllAuthorDto.add(authorDto);
            }

            message = "read all Authors success";
            result.put("Data", listAllAuthorDto);
        } else {
            responseStatus = HttpStatus.NO_CONTENT;
            message = "Author list is empty";
        }

        result.put("Message", message);
        result.put("Total", listAllAuthorDto.size());
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Get a single author
    @GetMapping("/authors/{authorId}")
    public ResponseEntity<Object> getAuthorByIdUrl(@PathVariable(value = "authorId") Long authorId) throws ResourceNotFoundException {
        return getAuthorById(authorId);
    }

    @GetMapping("/authors/param")
    public ResponseEntity<Object> getAuthorByIdParam(@RequestParam(name = "authorId") Long authorId) throws ResourceNotFoundException {
        return getAuthorById(authorId);
    }

    public ResponseEntity<Object> getAuthorById(Long authorId) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (authorRepository.findById(authorId).isPresent()) {
            Author authorEntity = authorRepository.findById(authorId).get();
            AuthorDto authorDto = modelMapper.map(authorEntity, AuthorDto.class);

            message = "get Author success";
            result.put("Data", authorDto);
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Author with Id " + authorId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Post author
    @PostMapping("/authors")
    public ResponseEntity<Object> createAuthor(@Valid @RequestBody Author author) {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        authorRepository.save(author);

        List<Author> listAllAuthors = authorRepository.findAll();

        if (listAllAuthors.contains(author)) {
            AuthorDto authorDto = modelMapper.map(author, AuthorDto.class);

            message = "create Author success";
            result.put("Data", authorDto);
        } else {
            responseStatus = HttpStatus.SERVICE_UNAVAILABLE;
            message = "create Author was failed";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Update author
    @PutMapping("/authors/{authorId}")
    public ResponseEntity<Object> updateAuthorUrl(
            @PathVariable(value = "authorId") Long authorId,
            @Valid @RequestBody Author authorDetails
    ) throws ResourceNotFoundException {
        return updateAuthor(authorId, authorDetails);
    }

    @PutMapping("/authors/param")
    public ResponseEntity<Object> updateAuthorParam(
            @RequestParam(name = "authorId") Long authorId,
            @Valid @RequestBody Author authorDetails
    ) throws ResourceNotFoundException {
        return updateAuthor(authorId, authorDetails);
    }

    public ResponseEntity<Object> updateAuthor(Long authorId, Author authorDetails) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (authorRepository.findById(authorId).isPresent()) {
            authorDetails.setAuthorId(authorId);

            Author authorEntity = modelMapper.map(authorDetails, Author.class);

            Author updatedAuthorEntity = authorRepository.save(authorEntity);

            List<Author> listAllAuthors = authorRepository.findAll();

            if (listAllAuthors.contains(updatedAuthorEntity)) {
                AuthorDto updatedAuthorDto = modelMapper.map(updatedAuthorEntity, AuthorDto.class);

                message = "update Author success";
                result.put("Data", updatedAuthorDto);
            } else {
                responseStatus = HttpStatus.SERVICE_UNAVAILABLE;
                message = "update Author was failed";
            }
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Author with Id " + authorId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Delete author
    @DeleteMapping("/authors/{authorId}")
    public ResponseEntity<Object> deleteAuthorUrl(@PathVariable(value = "authorId") Long authorId) throws ResourceNotFoundException {
        return deleteAuthor(authorId);
    }

    @DeleteMapping("/authors/param")
    public ResponseEntity<Object> deleteAuthorParam(@RequestParam(name = "authorId") Long authorId) throws ResourceNotFoundException {
        return deleteAuthor(authorId);
    }

    public ResponseEntity<Object> deleteAuthor(Long authorId) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (authorRepository.findById(authorId).isPresent()) {
            Author authorEntity = authorRepository.findById(authorId).get();

            authorRepository.delete(authorEntity);

            message = "Author with Id " + authorEntity.getAuthorId() + " was successfully deleted";
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Author with Id " + authorId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }
}
