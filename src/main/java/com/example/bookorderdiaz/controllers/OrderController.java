package com.example.bookorderdiaz.controllers;

import com.example.bookorderdiaz.dtos.CustomerDto;
import com.example.bookorderdiaz.dtos.OrderDto;
import com.example.bookorderdiaz.exceptions.ResourceNotFoundException;
import com.example.bookorderdiaz.models.Customer;
import com.example.bookorderdiaz.models.Order;
import com.example.bookorderdiaz.models.OrderBookDetails;
import com.example.bookorderdiaz.models.OrderDetailsKey;
import com.example.bookorderdiaz.repositories.BookRepository;
import com.example.bookorderdiaz.repositories.CustomerRepository;
import com.example.bookorderdiaz.repositories.OrderBookDetailsRepository;
import com.example.bookorderdiaz.repositories.OrderRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class OrderController {
    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private OrderBookDetailsRepository orderBookDetailsRepository;

    @Autowired
    private BookRepository bookRepository;

    // Get all order
    @GetMapping("/orders")
    public ResponseEntity<Object> getAllOrders() {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";
        List<Order> listAllOrderEntity = orderRepository.findAll();
        List<OrderDto> listAllOrderDto = new ArrayList<>();

        if (listAllOrderEntity.size() >= 1) {
            for (Order order: listAllOrderEntity) {
                OrderDto orderDto = modelMapper.map(order, OrderDto.class);
                listAllOrderDto.add(orderDto);
            }

            message = "read all Orders success";
            result.put("Data", listAllOrderDto);
        } else {
            responseStatus = HttpStatus.NO_CONTENT;
            message = "Order list is empty";
        }

        result.put("Message", message);
        result.put("Total", listAllOrderDto.size());
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Get a single order
    @GetMapping("/orders/{orderId}")
    public ResponseEntity<Object> getOrderByIdUrl(@PathVariable(value = "orderId") Long orderId) throws ResourceNotFoundException {
        return getOrderById(orderId);
    }

    @GetMapping("/orders/param")
    public ResponseEntity<Object> getOrderByIdParam(@RequestParam(name = "orderId") Long orderId) throws ResourceNotFoundException {
        return getOrderById(orderId);
    }

    public ResponseEntity<Object> getOrderById(Long orderId) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (orderRepository.findById(orderId).isPresent()) {
            Order orderEntity = orderRepository.findById(orderId).get();
            OrderDto orderDto = modelMapper.map(orderEntity, OrderDto.class);
            CustomerDto customerDto = modelMapper.map(orderEntity.getCustomer(), CustomerDto.class);

            orderDto.setCustomer(customerDto);

            message = "get Publisher success";
            result.put("Data", orderDto);
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Order with Id " + orderId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Post order
    @PostMapping("/orders")
    public ResponseEntity<Object> createOrder(@Valid @RequestBody Order order) {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (customerRepository.findById(order.getCustomer().getCustomerId()).isPresent()) {
            Customer customer = customerRepository.findById(order.getCustomer().getCustomerId()).get();

            order.setCustomer(customer);
            List<OrderBookDetails> listOrderBookDetailsEntity = new ArrayList<>();

            order.setListOrderBookDetails(listOrderBookDetailsEntity);

            orderRepository.save(order);

            List<Order> listAllOrders = orderRepository.findAll();

            if (listAllOrders.contains(order)) {
                CustomerDto customerDto = modelMapper.map(order.getCustomer(), CustomerDto.class);
                OrderDto orderDto = modelMapper.map(order, OrderDto.class);
                orderDto.setCustomer(customerDto);

                message = "create Order success";
                result.put("Data", orderDto);
            } else {
                responseStatus = HttpStatus.SERVICE_UNAVAILABLE;
                message = "craete Order was failed";
            }
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Customer with Id " + order.getCustomer().getCustomerId() + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Post order
    @PostMapping("/orders/list")
    public ResponseEntity<Object> createOrderList(@Valid @RequestBody Order order) {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (customerRepository.findById(order.getCustomer().getCustomerId()).isPresent()) {
            Customer customer = customerRepository.findById(order.getCustomer().getCustomerId()).get();

            order.setCustomer(customer);
            List<OrderBookDetails> listAllOrderBookDetails = orderBookDetailsRepository.findAll();
            List<OrderBookDetails> listOrderBookDetailsEntity = new ArrayList<>();

            order.setListOrderBookDetails(listOrderBookDetailsEntity);

            Order orderEntity = orderRepository.save(order);

            List<Order> listAllOrders = orderRepository.findAll();

            for (OrderBookDetails orderBookDetails: orderEntity.getListOrderBookDetails()) {
                OrderDetailsKey orderDetailsKey = new OrderDetailsKey(orderEntity.getOrderId(), orderBookDetails.getBook().getBookId());

                orderBookDetails.setBook(bookRepository.findById(orderBookDetails.getBook().getBookId()).get());
                orderBookDetails.calculateTax();

                System.out.println("Book Id: " + orderBookDetails.getBook().getBookId());

                orderBookDetails.setOrderDetailsId(orderDetailsKey);
                orderBookDetails.setOrder(orderEntity);
                System.out.println("Order Id: " + orderEntity.getOrderId());
                System.out.println("Order Book Details' Order Id: " + orderBookDetails.getOrder().getOrderId());
                orderBookDetailsRepository.save(orderBookDetails);
            }

            BigDecimal discount = new BigDecimal(0);
            if (orderEntity.getListOrderBookDetails().size() >= 3) {
                for (OrderBookDetails orderBookDetailsLoop: orderEntity.getListOrderBookDetails()) {
                    discount = (BigDecimal.valueOf(orderBookDetailsLoop.getQuantity())
                            .multiply(orderBookDetailsLoop.getBook().getBookPrice())
                            .multiply(BigDecimal.valueOf(0.1)));
                    orderBookDetailsLoop.setDiscount(discount);
                    orderBookDetailsRepository.save(orderBookDetailsLoop);
                }
            } else {
                for (OrderBookDetails orderBookDetailsLoop: orderEntity.getListOrderBookDetails()) {
                    discount = BigDecimal.valueOf(0.0);
                    orderBookDetailsLoop.setDiscount(discount);
                    orderBookDetailsRepository.save(orderBookDetailsLoop);
                }
            }

            BigDecimal totalOrder = new BigDecimal(0);

            for (OrderBookDetails orderBookDetails: orderEntity.getListOrderBookDetails()) {
                BigDecimal totalDiscount = orderBookDetails.getDiscount(); // (orderBookDetails.getDiscount());
                BigDecimal totalTax = orderBookDetails.getTax();
                BigDecimal totalPrice = (BigDecimal.valueOf(orderBookDetails.getQuantity())
                        .multiply(orderBookDetails.getBook().getBookPrice()));

                totalOrder = totalOrder.add(totalPrice.add(totalTax)).subtract(totalDiscount);
            }

            orderEntity.setTotalOrder(totalOrder);
            orderRepository.save(orderEntity);

            if (listAllOrders.contains(order)) {
                CustomerDto customerDto = modelMapper.map(order.getCustomer(), CustomerDto.class);
                OrderDto orderDto = modelMapper.map(order, OrderDto.class);
                orderDto.setCustomer(customerDto);

                message = "create Order success";
                result.put("Data", orderDto);
            } else {
                responseStatus = HttpStatus.SERVICE_UNAVAILABLE;
                message = "craete Order was failed";
            }
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Customer with Id " + order.getCustomer().getCustomerId() + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Update order
    @PutMapping("/orders/{orderId}")
    public ResponseEntity<Object> updateOrderUrl(
            @PathVariable(value = "orderId") Long orderId,
            @Valid @RequestBody Order orderDetails
    ) throws ResourceNotFoundException {
        return updateOrder(orderId, orderDetails);
    }

    @PutMapping("/orders/param")
    public ResponseEntity<Object> updateOrderParam(
            @RequestParam(name = "orderId") Long orderId,
            @Valid @RequestBody Order orderDetails
    ) throws ResourceNotFoundException {
        return updateOrder(orderId, orderDetails);
    }

    public ResponseEntity<Object> updateOrder(Long orderId, Order orderDetails) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (orderRepository.findById(orderId).isPresent()) {
            Order orderEntity = orderRepository.findById(orderId).get();

            orderEntity.setOrderDate(orderDetails.getOrderDate());

            Order updatedOrderEntity = orderRepository.save(orderEntity);

            List<Order> listAllOrders = orderRepository.findAll();

            if (listAllOrders.contains(updatedOrderEntity)) {
                CustomerDto customerDto = modelMapper.map(updatedOrderEntity.getCustomer(), CustomerDto.class);

                OrderDto updatedOrderDto = modelMapper.map(updatedOrderEntity, OrderDto.class);

                updatedOrderDto.setCustomer(customerDto);

                message = "update Order success";
                result.put("Data", updatedOrderDto);
            } else {
                responseStatus = HttpStatus.SERVICE_UNAVAILABLE;
                message = "update Order was failed";
            }
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Order with Id " + orderId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Delete order
    @DeleteMapping("/orders/{orderId}")
    public ResponseEntity<Object> deleteOrderUrl(@PathVariable(value = "orderId") Long orderId) throws ResourceNotFoundException {
        return deleteOrder(orderId);
    }

    @DeleteMapping("/orders/param")
    public ResponseEntity<Object> deleteOrderParam(@RequestParam(name = "orderId") Long orderId) throws ResourceNotFoundException {
        return deleteOrder(orderId);
    }

    public ResponseEntity<Object> deleteOrder(Long orderId) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (orderRepository.findById(orderId).isPresent()) {
            Order orderEntity = orderRepository.findById(orderId).get();

            orderRepository.delete(orderEntity);

            message = "Order with Id " + orderEntity.getOrderId() + " was successfully deleted";
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Order with Id " + orderId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

//    public BigDecimal countDiscount(Order order) {
//        BigDecimal discount = new BigDecimal(0);
//        if (order.getListOrderBookDetails().size() >= 3) {
//            for (OrderBookDetails orderBookDetails: order.getListOrderBookDetails()) {
//                discount = (BigDecimal.valueOf(orderBookDetails.getQuantity())
//                        .multiply(orderBookDetails.getBook().getBookPrice())
//                        .multiply(BigDecimal.valueOf(0.1)));
//            }
//        } else {
//            for (OrderBookDetails orderBookDetails: order.getListOrderBookDetails()) {
//                discount = BigDecimal.valueOf(0.0);
//            }
//        }
//
//        return discount;
//    }
//
//    public void countTotalOrder(OrderRepository orderRepository) {
//        List<Order> listAllOrder = orderRepository.findAll();
//
//        for (Order order: listAllOrder) {
//            BigDecimal totalOrder = new BigDecimal(0);
//            BigDecimal totalDiscount = new BigDecimal(0);
//            BigDecimal totalTax = new BigDecimal(0);
//            BigDecimal totalPrice = new BigDecimal(0);
//
//            if (!order.getListOrderBookDetails().isEmpty()) {
//                for (OrderBookDetails orderBookDetails: order.getListOrderBookDetails()) {
//                    totalDiscount = BigDecimal.valueOf(0); // (orderBookDetails.getDiscount());
//                    totalTax = (orderBookDetails.getTax());
//                    totalPrice = (BigDecimal.valueOf(orderBookDetails.getQuantity())
//                            .multiply(orderBookDetails.getBook().getBookPrice()));
//
//                    totalOrder = totalOrder.add(totalPrice.add(totalTax)).subtract(totalDiscount);
//                }
//            }
//            order.setTotalOrder(totalOrder);
//            orderRepository.save(order);
//        }
//    }
}
