package com.example.bookorderdiaz.controllers;

import com.example.bookorderdiaz.dtos.BookDto;
import com.example.bookorderdiaz.dtos.BookRatingDto;
import com.example.bookorderdiaz.dtos.ReviewerDto;
import com.example.bookorderdiaz.exceptions.ResourceNotFoundException;
import com.example.bookorderdiaz.models.Book;
import com.example.bookorderdiaz.models.BookRating;
import com.example.bookorderdiaz.models.Reviewer;
import com.example.bookorderdiaz.repositories.BookRatingRepository;
import com.example.bookorderdiaz.repositories.BookRepository;
import com.example.bookorderdiaz.repositories.ReviewerRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class BookRatingController {
    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private BookRatingRepository bookRatingRepository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private ReviewerRepository reviewerRepository;

    // Get all book rating
    @GetMapping("/book-ratings")
    public ResponseEntity<Object> getAllBookRatings() {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";
        List<BookRating> listAllBookRatingEntity = bookRatingRepository.findAll();
        List<BookRatingDto> listAllBookRatingDto = new ArrayList<>();

        if (listAllBookRatingEntity.size() >= 1) {
            for (BookRating bookRating: listAllBookRatingEntity) {
                if (bookRepository.findById(bookRating.getBook().getBookId()).isPresent() && reviewerRepository.findById(bookRating.getReviewer().getReviewerId()).isPresent()) {
                    Book book = bookRepository.findById(bookRating.getBook().getBookId()).get();
                    Reviewer reviewer = reviewerRepository.findById(bookRating.getReviewer().getReviewerId()).get();

                    bookRating.setBook(book);
                    bookRating.setReviewer(reviewer);

                    BookRatingDto bookRatingDto = modelMapper.map(bookRating, BookRatingDto.class);
                    BookDto bookDto = modelMapper.map(bookRating.getBook(), BookDto.class);
                    ReviewerDto reviewerDto = modelMapper.map(bookRating.getReviewer(), ReviewerDto.class);

                    bookRatingDto.setBook(bookDto);
                    bookRatingDto.setReviewer(reviewerDto);

                    listAllBookRatingDto.add(bookRatingDto);
                } else {
                    responseStatus = HttpStatus.NOT_FOUND;
                    message = "Book or Reviewer is not found";
                }
            }

            message = "read all Book Ratings success";
            result.put("Data", listAllBookRatingDto);
        } else {
            responseStatus = HttpStatus.NO_CONTENT;
            message = "Book Rating list is empty";
        }

        result.put("Message", message);
        result.put("Total", listAllBookRatingDto.size());
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Get a single book rating
    @GetMapping("/book-ratings/{bookRatingId}")
    public ResponseEntity<Object> getBookRatingByIdUrl(@PathVariable(value = "bookRatingId") Long bookRatingId) throws ResourceNotFoundException {
        return getBookRatingById(bookRatingId);
    }

    @GetMapping("/book-ratings/param")
    public ResponseEntity<Object> getBookRatingByIdParam(@RequestParam(name = "bookRatingId") Long bookRatingId) throws ResourceNotFoundException {
        return getBookRatingById(bookRatingId);
    }

    public ResponseEntity<Object> getBookRatingById(Long bookRatingId) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (bookRatingRepository.findById(bookRatingId).isPresent()) {
            BookRating bookRatingEntity = bookRatingRepository.findById(bookRatingId).get();

            if (bookRepository.findById(bookRatingEntity.getBook().getBookId()).isPresent() && reviewerRepository.findById(bookRatingEntity.getReviewer().getReviewerId()).isPresent()) {
                Book book = bookRepository.findById(bookRatingEntity.getBook().getBookId()).get();
                Reviewer reviewer = reviewerRepository.findById(bookRatingEntity.getReviewer().getReviewerId()).get();

                bookRatingEntity.setBook(book);
                bookRatingEntity.setReviewer(reviewer);

                BookRatingDto bookRatingDto = modelMapper.map(bookRatingEntity, BookRatingDto.class);
                BookDto bookDto = modelMapper.map(bookRatingEntity.getBook(), BookDto.class);
                ReviewerDto reviewerDto = modelMapper.map(bookRatingEntity.getReviewer(), ReviewerDto.class);

                bookRatingDto.setBook(bookDto);
                bookRatingDto.setReviewer(reviewerDto);

                message = "get Book Rating success";
                result.put("Data", bookRatingDto);
            } else {
                responseStatus = HttpStatus.NOT_FOUND;
                message = "Book or Reviewer is not found";
            }
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Book Rating with Id " + bookRatingId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Post book rating
    @PostMapping("/book-ratings")
    public ResponseEntity<Object> createBookRating(@Valid @RequestBody BookRating bookRating) {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (bookRepository.findById(bookRating.getBook().getBookId()).isPresent() && reviewerRepository.findById(bookRating.getReviewer().getReviewerId()).isPresent()) {
            Book book = bookRepository.findById(bookRating.getBook().getBookId()).get();
            Reviewer reviewer = reviewerRepository.findById(bookRating.getReviewer().getReviewerId()).get();

            bookRating.setBook(book);
            bookRating.setReviewer(reviewer);

            bookRatingRepository.save(bookRating);

            List<BookRating> listAllBookRatings = bookRatingRepository.findAll();

            if (listAllBookRatings.contains(bookRating)) {
                BookRatingDto bookRatingDto = modelMapper.map(bookRating, BookRatingDto.class);
                BookDto bookDto = modelMapper.map(bookRating.getBook(), BookDto.class);
                ReviewerDto reviewerDto = modelMapper.map(bookRating.getReviewer(), ReviewerDto.class);

                bookRatingDto.setBook(bookDto);
                bookRatingDto.setReviewer(reviewerDto);

                message = "create Book Rating success";
                result.put("Data", bookRatingDto);
            } else {
                responseStatus = HttpStatus.SERVICE_UNAVAILABLE;
                message = "create Book Rating was failed";
            }
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Book or Reviewer is not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Update book rating
    @PutMapping("/book-ratings/{bookRatingId}")
    public ResponseEntity<Object> updateBookRatingUrl(
            @PathVariable(value = "bookRatingId") Long bookRatingId,
            @Valid @RequestBody BookRating bookRatingDetails
    ) throws ResourceNotFoundException {
        return updateBookRating(bookRatingId, bookRatingDetails);
    }

    @PutMapping("/book-ratings/param")
    public ResponseEntity<Object> updateBookRatingParam(
            @RequestParam(name = "bookRatingId") Long bookRatingId,
            @Valid @RequestBody BookRating bookRatingDetails
    ) throws ResourceNotFoundException {
        return updateBookRating(bookRatingId, bookRatingDetails);
    }

    public ResponseEntity<Object> updateBookRating(Long bookRatingId, BookRating bookRatingDetails) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (bookRatingRepository.findById(bookRatingId).isPresent()) {
            BookRating bookRatingEntity = bookRatingRepository.findById(bookRatingId).get();

            if (bookRepository.findById(bookRatingEntity.getBook().getBookId()).isPresent() && reviewerRepository.findById(bookRatingEntity.getReviewer().getReviewerId()).isPresent()) {
                Book book = bookRepository.findById(bookRatingEntity.getBook().getBookId()).get();
                Reviewer reviewer = reviewerRepository.findById(bookRatingEntity.getReviewer().getReviewerId()).get();

                bookRatingEntity.setRatingScore(bookRatingDetails.getRatingScore());
                bookRatingEntity.setBook(book);
                bookRatingEntity.setReviewer(reviewer);

                BookRating updatedBookRatingEntity = bookRatingRepository.save(bookRatingEntity);

                List<BookRating> listAllBookRatings = bookRatingRepository.findAll();

                if (listAllBookRatings.contains(updatedBookRatingEntity)) {
                    BookRatingDto bookRatingDto = modelMapper.map(updatedBookRatingEntity, BookRatingDto.class);
                    BookDto bookDto = modelMapper.map(updatedBookRatingEntity.getBook(), BookDto.class);
                    ReviewerDto reviewerDto = modelMapper.map(updatedBookRatingEntity.getReviewer(), ReviewerDto.class);

                    bookRatingDto.setBook(bookDto);
                    bookRatingDto.setReviewer(reviewerDto);

                    message = "update Book Rating success";
                    result.put("Data", bookRatingDto);
                } else {
                    responseStatus = HttpStatus.SERVICE_UNAVAILABLE;
                    message = "update Book Rating was failed";
                }
            } else {
                responseStatus = HttpStatus.NOT_FOUND;
                message = "Book or Reviewer is not found";
            }
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Book Rating with Id " + bookRatingId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Delete book rating
    @DeleteMapping("/book-ratings/{bookRatingId}")
    public ResponseEntity<Object> deleteBookRatingUrl(@PathVariable(value = "bookRatingId") Long bookRatingId) throws ResourceNotFoundException {
        return deleteBookRating(bookRatingId);
    }

    @DeleteMapping("/book-ratings/param")
    public ResponseEntity<Object> deleteBookRatingParam(@RequestParam(name = "bookRatingId") Long bookRatingId) throws ResourceNotFoundException {
        return deleteBookRating(bookRatingId);
    }

    public ResponseEntity<Object> deleteBookRating(Long bookRatingId) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (bookRatingRepository.findById(bookRatingId).isPresent()) {
            BookRating bookRatingEntity = bookRatingRepository.findById(bookRatingId).get();

            bookRatingRepository.delete(bookRatingEntity);

            message = "Book Rating with Id " + bookRatingEntity.getBookRatingId() + " was successfully deleted";
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Book Rating with Id " + bookRatingId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }
}
