package com.example.bookorderdiaz.controllers;

import com.example.bookorderdiaz.dtos.ReviewerDto;
import com.example.bookorderdiaz.exceptions.ResourceNotFoundException;
import com.example.bookorderdiaz.models.Reviewer;
import com.example.bookorderdiaz.repositories.ReviewerRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class ReviewerController {
    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private ReviewerRepository reviewerRepository;

    // Get all reviewer
    @GetMapping("/reviewers")
    public ResponseEntity<Object> getAllReviewers() {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";
        List<Reviewer> listAllReviewerEntity = reviewerRepository.findAll();
        List<ReviewerDto> listAllReviewerDto = new ArrayList<>();

        if (listAllReviewerEntity.size() >= 1) {
            for (Reviewer reviewer: listAllReviewerEntity) {
                ReviewerDto reviewerDto = modelMapper.map(reviewer, ReviewerDto.class);

                listAllReviewerDto.add(reviewerDto);
            }

            message = "read all Reviewers success";
            result.put("Data", listAllReviewerDto);
        } else {
            responseStatus = HttpStatus.NO_CONTENT;
            message = "Reviewer list is empty";
        }

        result.put("Message", message);
        result.put("Total", listAllReviewerDto.size());
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Get a single reviewer
    @GetMapping("/reviewers/{reviewerId}")
    public ResponseEntity<Object> getReviewerByIdUrl(@PathVariable(value = "reviewerId") Long reviewerId) throws ResourceNotFoundException {
        return getReviewerById(reviewerId);
    }

    @GetMapping("/reviewers/param")
    public ResponseEntity<Object> getReviewerByIdParam(@RequestParam(name = "reviewerId") Long reviewerId) throws ResourceNotFoundException {
        return getReviewerById(reviewerId);
    }

    public ResponseEntity<Object> getReviewerById(Long reviewerId) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (reviewerRepository.findById(reviewerId).isPresent()) {
            Reviewer reviewerEntity = reviewerRepository.findById(reviewerId).get();
            ReviewerDto reviewerDto = modelMapper.map(reviewerEntity, ReviewerDto.class);

            message = "get Reviewer success";
            result.put("Data", reviewerDto);
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Reviewer with " + reviewerId + " Id was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Post reviewer
    @PostMapping("/reviewers")
    public ResponseEntity<Object> createReviewer(@Valid @RequestBody Reviewer reviewer) {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        reviewerRepository.save(reviewer);

        List<Reviewer> listAllReviewers = reviewerRepository.findAll();

        if (listAllReviewers.contains(reviewer)) {
            ReviewerDto reviewerDto = modelMapper.map(reviewer, ReviewerDto.class);

            message = "create Reviewer success";
            result.put("Data", reviewerDto);
        } else {
            responseStatus = HttpStatus.SERVICE_UNAVAILABLE;
            message = "create Reviewer was failed";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Update reviewer
    @PutMapping("/reviewers/{reviewerId}")
    public ResponseEntity<Object> updateReviewerUrl(
            @PathVariable(value = "reviewerId") Long reviewerId,
            @Valid @RequestBody Reviewer reviewerDetails
    ) throws ResourceNotFoundException {
        return updateReviewer(reviewerId, reviewerDetails);
    }

    @PutMapping("/reviewers/param")
    public ResponseEntity<Object> updateReviewerParam(
            @RequestParam(name = "reviewerId") Long reviewerId,
            @Valid @RequestBody Reviewer reviewerDetails
    ) throws ResourceNotFoundException {
        return updateReviewer(reviewerId, reviewerDetails);
    }

    public ResponseEntity<Object> updateReviewer(Long reviewerId, Reviewer reviewerDetails) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (reviewerRepository.findById(reviewerId).isPresent()) {
            Reviewer reviewerEntity = reviewerRepository.findById(reviewerId).get();

            reviewerEntity.setReviewerName(reviewerDetails.getReviewerName());
            reviewerEntity.setReviewerCountry(reviewerDetails.getReviewerCountry());
            reviewerEntity.setVerifiedStatus(reviewerDetails.getVerifiedStatus());

            Reviewer updatedReviewerEntity = reviewerRepository.save(reviewerEntity);

            List<Reviewer> listAllReviewers = reviewerRepository.findAll();

            if (listAllReviewers.contains(updatedReviewerEntity)) {
                ReviewerDto updatedReviewerDto = modelMapper.map(updatedReviewerEntity, ReviewerDto.class);

                message = "update Reviewer success";
                result.put("Data", updatedReviewerDto);
            } else {
                responseStatus = HttpStatus.SERVICE_UNAVAILABLE;
                message = "update Reviewer was failed";
            }
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Reviewer with " + reviewerId + " Id was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Delete reviewer
    @DeleteMapping("/reviewers/{reviewerId}")
    public ResponseEntity<Object> deleteReviewerUrl(@PathVariable(value = "reviewerId") Long reviewerId) throws ResourceNotFoundException {
        return deleteReviewer(reviewerId);
    }

    @DeleteMapping("/reviewers/param")
    public ResponseEntity<Object> deleteReviewerParam(@RequestParam(name = "reviewerId") Long reviewerId) throws ResourceNotFoundException {
        return deleteReviewer(reviewerId);
    }

    public ResponseEntity<Object> deleteReviewer(Long reviewerId) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (reviewerRepository.findById(reviewerId).isPresent()) {
            Reviewer reviewerEntity = reviewerRepository.findById(reviewerId).get();

            reviewerRepository.delete(reviewerEntity);

            message = "Reviewer with Id " + reviewerEntity.getReviewerId() + " was successfully deleted";
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Reviewer with " + reviewerId + " Id was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }
}
