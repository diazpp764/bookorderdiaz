package com.example.bookorderdiaz.controllers;

import com.example.bookorderdiaz.dtos.CustomerDto;
import com.example.bookorderdiaz.exceptions.ResourceNotFoundException;
import com.example.bookorderdiaz.models.Customer;
import com.example.bookorderdiaz.repositories.CustomerRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class CustomerController {
    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    CustomerRepository customerRepository;

    // Get all customer
    @GetMapping("/customers")
    public ResponseEntity<Object> getAllCustomers() {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";
        List<Customer> listAllCustomerEntity = customerRepository.findAll();
        List<CustomerDto> listAllCustomerDto = new ArrayList<>();

        if (listAllCustomerEntity.size() >= 1) {
            for (Customer customer: listAllCustomerEntity) {
                CustomerDto customerDto = modelMapper.map(customer, CustomerDto.class);

                listAllCustomerDto.add(customerDto);
            }

            message = "read all Customers success";
            result.put("Data", listAllCustomerDto);
        } else {
            responseStatus = HttpStatus.NO_CONTENT;
            message = "Customer list is empty";
        }

        result.put("Message", message);
        result.put("Total", listAllCustomerDto.size());
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Get a single customer
    @GetMapping("/customers/{customerId}")
    public ResponseEntity<Object> getCustomerByIdUrl(@PathVariable(value = "customerId") Long customerId) throws ResourceNotFoundException {
        return getCustomerById(customerId);
    }

    @GetMapping("/customers/param")
    public ResponseEntity<Object> getCustomerByIdParam(@RequestParam(name = "customerId") Long customerId) throws ResourceNotFoundException {
        return getCustomerById(customerId);
    }

    public ResponseEntity<Object> getCustomerById(Long customerId) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (customerRepository.findById(customerId).isPresent()) {
            Customer customerEntity = customerRepository.findById(customerId).get();
            CustomerDto customerDto = modelMapper.map(customerEntity, CustomerDto.class);

            message = "get Customer success";
            result.put("Data", customerDto);
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Customer with Id " + customerId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Post customer
    @PostMapping("/customers")
    public ResponseEntity<Object> createCustomer(@Valid @RequestBody Customer customer) {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        customerRepository.save(customer);

        List<Customer> listAllcustomers = customerRepository.findAll();

        if (listAllcustomers.contains(customer)) {
            CustomerDto customerDto = modelMapper.map(customer, CustomerDto.class);

            message = "create Customer success";
            result.put("Data", customerDto);
        } else {
            responseStatus = HttpStatus.SERVICE_UNAVAILABLE;
            message = "create Customer was failed";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Update customer
    @PutMapping("/customers/{customerId}")
    public ResponseEntity<Object> updateCustomerUrl(
            @PathVariable(value = "customerId") Long customerId,
            @Valid @RequestBody Customer customerDetails
    ) throws ResourceNotFoundException {
        return updateCustomer(customerId, customerDetails);
    }

    @PutMapping("/customers/param")
    public ResponseEntity<Object> updateCustomerParam(
            @RequestParam(name = "customerId") Long customerId,
            @Valid @RequestBody Customer customerDetails
    ) throws ResourceNotFoundException {
        return updateCustomer(customerId, customerDetails);
    }

    public ResponseEntity<Object> updateCustomer(Long customerId, Customer customerDetails) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (customerRepository.findById(customerId).isPresent()) {
            Customer customerEntity = customerRepository.findById(customerId).get();

            customerEntity.setCustomerName(customerDetails.getCustomerName());
            customerEntity.setCustomerCountry(customerDetails.getCustomerCountry());
            customerEntity.setAddress(customerDetails.getAddress());
            customerEntity.setPhoneNumber(customerDetails.getPhoneNumber());
            customerEntity.setPostalCode(customerDetails.getPostalCode());
            customerEntity.setEmail(customerDetails.getEmail());

            Customer updatedCustomerEntity = customerRepository.save(customerEntity);

            List<Customer> listAllcustomers = customerRepository.findAll();

            if (listAllcustomers.contains(updatedCustomerEntity)) {
                CustomerDto updatedCustomerDto = modelMapper.map(updatedCustomerEntity, CustomerDto.class);

                message = "update Customer success";
                result.put("Data", updatedCustomerDto);
            } else {
                responseStatus = HttpStatus.SERVICE_UNAVAILABLE;
                message = "update Customer was failed";
            }
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Customer with Id " + customerId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Delete customer
    @DeleteMapping("/customers/{customerId}")
    public ResponseEntity<Object> deleteCustomerUrl(@PathVariable(value = "customerId") Long customerId) throws ResourceNotFoundException {
        return deleteCustomer(customerId);
    }

    @DeleteMapping("/customers/param")
    public ResponseEntity<Object> deleteCustomerParam(@RequestParam(name = "customerId") Long customerId) throws ResourceNotFoundException {
        return deleteCustomer(customerId);
    }

    public ResponseEntity<Object> deleteCustomer(Long customerId) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (customerRepository.findById(customerId).isPresent()) {
            Customer customerEntity = customerRepository.findById(customerId).get();

            customerRepository.delete(customerEntity);

            message = "Customer with Id " + customerEntity.getCustomerId() + " was successfully deleted";
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Customer with Id " + customerId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

}
