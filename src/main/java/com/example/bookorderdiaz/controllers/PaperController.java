package com.example.bookorderdiaz.controllers;

import com.example.bookorderdiaz.dtos.PaperDto;
import com.example.bookorderdiaz.exceptions.ResourceNotFoundException;
import com.example.bookorderdiaz.models.Paper;
import com.example.bookorderdiaz.repositories.PaperRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class PaperController {
    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private PaperRepository paperRepository;

    // Get all paper
    @GetMapping("/papers")
    public ResponseEntity<Object> getAllPapers() {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";
        List<Paper> listAllPaperEntity = paperRepository.findAll();
        List<PaperDto> listAllPaperDto = new ArrayList<>();

        if (listAllPaperEntity.size() >= 1) {
            for (Paper paper: listAllPaperEntity) {
                PaperDto paperDto = modelMapper.map(paper, PaperDto.class);

                listAllPaperDto.add(paperDto);
            }

            message = "read all Papers success";
            result.put("Data", listAllPaperDto);
        } else {
            responseStatus = HttpStatus.NO_CONTENT;
            message = "Paper list is empty";
        }

        result.put("Message", message);
        result.put("Total", listAllPaperDto.size());
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Get a single paper
    @GetMapping("/papers/{paperId}")
    public ResponseEntity<Object> getPaperByIdUrl(@PathVariable(value = "paperId") Long paperId) throws ResourceNotFoundException {
        return getPaperById(paperId);
    }

    @GetMapping("/papers/param")
    public ResponseEntity<Object> getPaperByIdParam(@RequestParam(name = "paperId") Long paperId) throws ResourceNotFoundException {
        return getPaperById(paperId);
    }

    public ResponseEntity<Object> getPaperById(Long paperId) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (paperRepository.findById(paperId).isPresent()) {
            Paper paperEntity = paperRepository.findById(paperId).get();
            PaperDto paperDto = modelMapper.map(paperEntity, PaperDto.class);

            message = "get Paper success";
            result.put("Data", paperDto);
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Paper with Id " + paperId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Post paper
    @PostMapping("/papers")
    public ResponseEntity<Object> createPaper(@Valid @RequestBody Paper paper) {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        paperRepository.save(paper);

        List<Paper> listAllPapers = paperRepository.findAll();

        if (listAllPapers.contains(paper)) {
            PaperDto paperDto = modelMapper.map(paper, PaperDto.class);

            message = "create Paper success";
            result.put("Data", paperDto);
        } else {
            responseStatus = HttpStatus.SERVICE_UNAVAILABLE;
            message = "create Paper was failed";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Update paper
    @PutMapping("/papers/{paperId}")
    public ResponseEntity<Object> updatePaperUrl(
            @PathVariable(value = "paperId") Long paperId,
            @Valid @RequestBody Paper paperDetails
    ) throws ResourceNotFoundException {
        return updatePaper(paperId, paperDetails);
    }

    @PutMapping("/papers/param")
    public ResponseEntity<Object> updatePaperParam(
            @RequestParam(name = "paperId") Long paperId,
            @Valid @RequestBody Paper paperDetails
    ) throws ResourceNotFoundException {
        return updatePaper(paperId, paperDetails);
    }

    public ResponseEntity<Object> updatePaper(Long paperId, Paper paperDetails) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (paperRepository.findById(paperId).isPresent()) {
            Paper paperEntity = paperRepository.findById(paperId).get();

            paperEntity.setQualityName(paperDetails.getQualityName());
            paperEntity.setPaperPrice(paperDetails.getPaperPrice());

            Paper updatedPaperEntity = paperRepository.save(paperEntity);

            List<Paper> listAllPapers = paperRepository.findAll();

            if (listAllPapers.contains(updatedPaperEntity)) {
                PaperDto updatedPaperDto = modelMapper.map(updatedPaperEntity, PaperDto.class);

                message = "update Paper success";
                result.put("Data", updatedPaperDto);
            } else {
                responseStatus = HttpStatus.SERVICE_UNAVAILABLE;
                message = "update Paper was failed";
            }
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Paper with Id " + paperId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Delete paper
    @DeleteMapping("/papers/{paperId}")
    public ResponseEntity<Object> deletePaperUrl(@PathVariable(value = "paperId") Long paperId) throws ResourceNotFoundException {
        return deletePaper(paperId);
    }

    @DeleteMapping("/papers/param")
    public ResponseEntity<Object> deletePaperParam(@RequestParam(name = "paperId") Long paperId) throws ResourceNotFoundException {
        return deletePaper(paperId);
    }

    public ResponseEntity<Object> deletePaper(Long paperId) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (paperRepository.findById(paperId).isPresent()) {
            Paper paperEntity = paperRepository.findById(paperId).get();

            paperRepository.delete(paperEntity);

            message = "Paper with Id " + paperEntity.getPaperId() + " was successfully deleted";
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Paper with Id " + paperId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }
}
