package com.example.bookorderdiaz.controllers;

import com.example.bookorderdiaz.dtos.PaperDto;
import com.example.bookorderdiaz.dtos.PublisherDto;
import com.example.bookorderdiaz.exceptions.ResourceNotFoundException;
import com.example.bookorderdiaz.models.Paper;
import com.example.bookorderdiaz.models.Publisher;
import com.example.bookorderdiaz.repositories.PaperRepository;
import com.example.bookorderdiaz.repositories.PublisherRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class PublisherController {
    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private PublisherRepository publisherRepository;

    @Autowired
    private PaperRepository paperRepository;

    // Get all publisher
    @GetMapping("/publishers")
    public ResponseEntity<Object> getAllPublishers() {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";
        List<Publisher> listAllPublisherEntity = publisherRepository.findAll();
        List<PublisherDto> listAllPublisherDto = new ArrayList<>();

        if (listAllPublisherEntity.size() >= 1) {
            for (Publisher publisher: listAllPublisherEntity) {
                PublisherDto publisherDto = modelMapper.map(publisher, PublisherDto.class);
                PaperDto paperDto = modelMapper.map(publisher.getPaper(), PaperDto.class);

                publisherDto.setPaper(paperDto);

                listAllPublisherDto.add(publisherDto);
            }

            message = "read all Publishers success";
            result.put("Data", listAllPublisherDto);
        } else {
            responseStatus = HttpStatus.NO_CONTENT;
            message = "Publisher list is empty";
        }

        result.put("Message", message);
        result.put("Total", listAllPublisherDto.size());
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Get a single publisher
    @GetMapping("/publishers/{publisherId}")
    public ResponseEntity<Object> getPublisherByIdUrl(@PathVariable(value = "publisherId") Long publisherId) throws ResourceNotFoundException {
        return getPublisherById(publisherId);
    }

    @GetMapping("/publishers/param")
    public ResponseEntity<Object> getPublisherByIdParam(@RequestParam(name = "publisherId") Long publisherId) throws ResourceNotFoundException {
        return getPublisherById(publisherId);
    }

    public ResponseEntity<Object> getPublisherById(Long publisherId) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (publisherRepository.findById(publisherId).isPresent()) {
            Publisher publisherEntity = publisherRepository.findById(publisherId).get();
            PublisherDto publisherDto = modelMapper.map(publisherEntity, PublisherDto.class);
            PaperDto paperDto = modelMapper.map(publisherEntity.getPaper(), PaperDto.class);

            publisherDto.setPaper(paperDto);

            message = "get Publisher success";
            result.put("Data", publisherDto);
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Publisher with Id " + publisherId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Post publisher
    @PostMapping("/publishers")
    public ResponseEntity<Object> createPublisher(@Valid @RequestBody Publisher publisher) {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (paperRepository.findById(publisher.getPaper().getPaperId()).isPresent()) {
            Paper paper = paperRepository.findById(publisher.getPaper().getPaperId()).get();

            publisher.setPaper(paper);

            publisherRepository.save(publisher);

            List<Publisher> listAllPublishers = publisherRepository.findAll();

            if (listAllPublishers.contains(publisher)) {
                PaperDto paperDto = modelMapper.map(publisher.getPaper(), PaperDto.class);

                PublisherDto publisherDto = modelMapper.map(publisher, PublisherDto.class);

                publisherDto.setPaper(paperDto);

                message = "create Publisher success";
                result.put("Data", publisherDto);
            } else {
                responseStatus = HttpStatus.SERVICE_UNAVAILABLE;
                message = "create Publisher was failed";
            }
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Paper with Id " + publisher.getPaper().getPaperId() + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Update publisher
    @PutMapping("/publishers/{publisherId}")
    public ResponseEntity<Object> updatePublisherUrl(
            @PathVariable(value = "publisherId") Long publisherId,
            @Valid @RequestBody Publisher publisherDetails
    ) throws ResourceNotFoundException {
        return updatePublisher(publisherId, publisherDetails);
    }

    @PutMapping("/publishers/param")
    public ResponseEntity<Object> updatePublisherParam(
            @RequestParam(name = "publisherId") Long publisherId,
            @Valid @RequestBody Publisher publisherDetails
    ) throws ResourceNotFoundException {
        return updatePublisher(publisherId, publisherDetails);
    }

    public ResponseEntity<Object> updatePublisher(Long publisherId, Publisher publisherDetails) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (publisherRepository.findById(publisherId).isPresent()) {
            Publisher publisherEntity = publisherRepository.findById(publisherId).get();

            publisherEntity.setCompanyName(publisherDetails.getCompanyName());
            publisherEntity.setPublisherCountry(publisherDetails.getPublisherCountry());

            Publisher updatedPublisherEntity = publisherRepository.save(publisherEntity);

            List<Publisher> listAllPublishers = publisherRepository.findAll();

            if (listAllPublishers.contains(updatedPublisherEntity)) {
                PaperDto paperDto = modelMapper.map(updatedPublisherEntity.getPaper(), PaperDto.class);

                PublisherDto updatedPublisherDto = modelMapper.map(updatedPublisherEntity, PublisherDto.class);

                updatedPublisherDto.setPaper(paperDto);

                message = "update Publisher success";
                result.put("Data", updatedPublisherDto);
            } else {
                responseStatus = HttpStatus.SERVICE_UNAVAILABLE;
                message = "update Publisher was failed";
            }
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Publisher with Id " + publisherId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Delete publisher
    @DeleteMapping("/publishers/{publisherId}")
    public ResponseEntity<Object> deletePublisherUrl(@PathVariable(value = "publisherId") Long publisherId) throws ResourceNotFoundException {
        return deletePublisher(publisherId);
    }

    @DeleteMapping("/publishers/param")
    public ResponseEntity<Object> deletePublisherParam(@RequestParam(name = "publisherId") Long publisherId) throws ResourceNotFoundException {
        return deletePublisher(publisherId);
    }

    public ResponseEntity<Object> deletePublisher(Long publisherId) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (publisherRepository.findById(publisherId).isPresent()) {
            Publisher publisherEntity = publisherRepository.findById(publisherId).get();

            publisherRepository.delete(publisherEntity);

            message = "Publisher with Id " + publisherEntity.getPublisherId() + " was successfully deleted";
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Publisher with Id " + publisherId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }
}
