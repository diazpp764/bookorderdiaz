package com.example.bookorderdiaz.controllers;

import com.example.bookorderdiaz.dtos.BookDto;
import com.example.bookorderdiaz.dtos.OrderBookDetailsDto;
import com.example.bookorderdiaz.dtos.OrderDto;
import com.example.bookorderdiaz.exceptions.ResourceNotFoundException;
import com.example.bookorderdiaz.models.Book;
import com.example.bookorderdiaz.models.Order;
import com.example.bookorderdiaz.models.OrderBookDetails;
import com.example.bookorderdiaz.models.OrderDetailsKey;
import com.example.bookorderdiaz.repositories.BookRepository;
import com.example.bookorderdiaz.repositories.OrderBookDetailsRepository;
import com.example.bookorderdiaz.repositories.OrderRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class OrderBookDetailsController {
    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private OrderBookDetailsRepository orderBookDetailsRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private BookRepository bookRepository;

    // Get all order book details
    @GetMapping("/order-book-details")
    public ResponseEntity<Object> getAllOrderBookDetails() {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";
        List<OrderBookDetails> listAllOrderBookDetailsEntity = orderBookDetailsRepository.findAll();
        List<OrderBookDetailsDto> listAllOrderBookDetailsDto = new ArrayList<>();

        if (listAllOrderBookDetailsEntity.size() >= 1) {
            for (OrderBookDetails orderBookDetails: listAllOrderBookDetailsEntity) {
                OrderBookDetailsDto orderBookDetailsDto = modelMapper.map(orderBookDetails, OrderBookDetailsDto.class);
                OrderDto orderDto = modelMapper.map(orderBookDetails.getOrder(), OrderDto.class);
                BookDto bookDto = modelMapper.map(orderBookDetails.getBook(), BookDto.class);

                orderBookDetailsDto.setOrder(orderDto);
                orderBookDetailsDto.setBook(bookDto);

                listAllOrderBookDetailsDto.add(orderBookDetailsDto);
            }

            message = "read all Order Book Details success";
            result.put("Data", listAllOrderBookDetailsDto);
        } else {
            responseStatus = HttpStatus.NO_CONTENT;
            message = "Order Book Details list is empty";
        }

        result.put("Message", message);
        result.put("Total", listAllOrderBookDetailsDto.size());
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Get a single order book details
    @GetMapping("/orders/{orderId}/order-book-details/{bookId}")
    public ResponseEntity<Object> getOrderBookDetailsByIdUrl(
            @PathVariable(value = "orderId") Long orderId,
            @PathVariable(value = "bookId") Long bookId
    ) throws ResourceNotFoundException {
        OrderDetailsKey orderDetailsKey = new OrderDetailsKey(orderId, bookId);

        return getOrderBookDetailsById(orderDetailsKey);
    }

    @GetMapping("/orders/{orderId}/order-book-details/param")
    public ResponseEntity<Object> getOrderBookDetailsByIdParam(
            @PathVariable(value = "orderId") Long orderId,
            @RequestParam(name = "bookId") Long bookId
    ) throws ResourceNotFoundException {
        OrderDetailsKey orderDetailsKey = new OrderDetailsKey(orderId, bookId);

        return getOrderBookDetailsById(orderDetailsKey);
    }

    public ResponseEntity<Object> getOrderBookDetailsById(OrderDetailsKey orderBookDetailsId) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";
        Boolean isPresent = false;
        OrderBookDetails orderBookDetailsEntity = new OrderBookDetails();

        List<OrderBookDetails> listAllOrderBookDetails = orderBookDetailsRepository.findAll();

        for (OrderBookDetails orderBookDetails: listAllOrderBookDetails) {
            if (orderBookDetails.getOrderDetailsId().equals(orderBookDetailsId)) {
                orderBookDetailsEntity = modelMapper.map(orderBookDetails, OrderBookDetails.class);
                isPresent = true;
            }
        }

        if (isPresent) {
            OrderBookDetailsDto orderBookDetailsDto = modelMapper.map(orderBookDetailsEntity, OrderBookDetailsDto.class);
            OrderDto orderDto = modelMapper.map(orderBookDetailsEntity.getOrder(), OrderDto.class);
            BookDto bookDto = modelMapper.map(orderBookDetailsEntity.getBook(), BookDto.class);

            orderBookDetailsDto.setOrder(orderDto);
            orderBookDetailsDto.setBook(bookDto);

            message = "get Order Book Details success";
            result.put("Data", orderBookDetailsDto);
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Order Book Details with Id " + orderBookDetailsId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Post order book details
    @PostMapping("/order-book-details")
    public ResponseEntity<Object> createOrderBookDetails(@Valid @RequestBody OrderBookDetails orderBookDetails) {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        OrderBookDetails savedOrderBookDetails = new OrderBookDetails();

        OrderDetailsKey orderDetailsKey = new OrderDetailsKey(orderBookDetails.getOrder().getOrderId(), orderBookDetails.getBook().getBookId());

        orderBookDetails.setOrderDetailsId(orderDetailsKey);

        if (bookRepository.findById(orderBookDetails.getBook().getBookId()).isPresent() && orderRepository.findById(orderBookDetails.getOrder().getOrderId()).isPresent()) {
            Book book = bookRepository.findById(orderBookDetails.getBook().getBookId()).get();
            Order order = orderRepository.findById(orderBookDetails.getOrder().getOrderId()).get();

            orderBookDetails.setBook(book);
            orderBookDetails.setOrder(order);
            orderBookDetails.calculateTax();
            orderBookDetails.setDiscount(countDiscount(order));

            savedOrderBookDetails = orderBookDetailsRepository.save(orderBookDetails);
            countTotalOrder(orderRepository);

            OrderBookDetailsDto orderBookDetailsDto = modelMapper.map(savedOrderBookDetails, OrderBookDetailsDto.class);
            OrderDto orderDto = modelMapper.map(savedOrderBookDetails.getOrder(), OrderDto.class);
            BookDto bookDto = modelMapper.map(savedOrderBookDetails.getBook(), BookDto.class);

            orderBookDetailsDto.setOrder(orderDto);
            orderBookDetailsDto.setBook(bookDto);

            message = "create Order Book Details success";
            result.put("Data", orderBookDetailsDto);
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Order or Book Id was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Update order book details
    @PutMapping("/orders/{orderId}/order-book-details/{bookId}")
    public ResponseEntity<Object> updateOrderBookDetailsUrl(
            @PathVariable(value = "orderId") Long orderId,
            @PathVariable(value = "bookId") Long bookId,
            @Valid @RequestBody OrderBookDetails orderBookDetails
    ) throws ResourceNotFoundException {
        OrderDetailsKey orderDetailsKey = new OrderDetailsKey(orderId, bookId);

        return updateOrderBookDetails(orderDetailsKey, orderBookDetails);
    }

    @PutMapping("/orders/{orderId}/order-book-details/param")
    public ResponseEntity<Object> updateOrderBookDetailsParam(
            @PathVariable(value = "orderId") Long orderId,
            @RequestParam(name = "bookId") Long bookId,
            @Valid @RequestBody OrderBookDetails orderBookDetails
    ) throws ResourceNotFoundException {
        OrderDetailsKey orderDetailsKey = new OrderDetailsKey(orderId, bookId);

        return updateOrderBookDetails(orderDetailsKey, orderBookDetails);
    }

    public ResponseEntity<Object> updateOrderBookDetails(OrderDetailsKey orderBookDetailsId, OrderBookDetails orderBookDetails) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";
        Boolean isPresent = false;
        OrderBookDetails orderBookDetailsEntity = new OrderBookDetails();
        OrderBookDetails updatedOrderBookDetailsEntity = new OrderBookDetails();

                List<OrderBookDetails> listAllOrderBookDetails = orderBookDetailsRepository.findAll();

        for (OrderBookDetails orderBookDetailsLoop: listAllOrderBookDetails) {
            if (orderBookDetailsLoop.getOrderDetailsId().equals(orderBookDetailsId)) {
                orderBookDetailsEntity = modelMapper.map(orderBookDetailsLoop, OrderBookDetails.class);
                isPresent = true;
            }
        }

        if (isPresent) {
            if (orderRepository.findById(orderBookDetailsEntity.getOrder().getOrderId()).isPresent() && bookRepository.findById(orderBookDetailsEntity.getBook().getBookId()).isPresent()) {
                Order order = orderRepository.findById(orderBookDetailsEntity.getOrder().getOrderId()).get();
                Book book = bookRepository.findById(orderBookDetailsEntity.getBook().getBookId()).get();

                orderBookDetailsEntity.setQuantity(orderBookDetails.getQuantity());
                orderBookDetailsEntity.setDiscount(orderBookDetails.getDiscount());
                orderBookDetailsEntity.calculateTax();
                orderBookDetailsEntity.setDiscount(countDiscount(order));

                updatedOrderBookDetailsEntity = orderBookDetailsRepository.save(orderBookDetailsEntity);
                countTotalOrder(orderRepository);

                if (listAllOrderBookDetails.contains(updatedOrderBookDetailsEntity)) {
                    OrderBookDetailsDto orderBookDetailsDto = modelMapper.map(updatedOrderBookDetailsEntity, OrderBookDetailsDto.class);
                    OrderDto orderDto = modelMapper.map(updatedOrderBookDetailsEntity.getOrder(), OrderDto.class);
                    BookDto bookDto = modelMapper.map(updatedOrderBookDetailsEntity.getBook(), BookDto.class);

                    orderBookDetailsDto.setOrder(orderDto);
                    orderBookDetailsDto.setBook(bookDto);

                    message = "update Order Book Details success";
                    result.put("Data", orderBookDetailsDto);
                } else {
                    responseStatus = HttpStatus.SERVICE_UNAVAILABLE;
                    message = "update Order Book Details was failed";
                }
            } else {
                responseStatus = HttpStatus.NOT_FOUND;
                message = "Order or Book is not found";
            }
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Order Book Details with Id " + orderBookDetailsId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Delete order book details
    @DeleteMapping("/orders/{orderId}/order-book-details/{bookId}")
    public ResponseEntity<Object> deleteOrderBookDetailsUrl(
            @PathVariable(value = "orderId") Long orderId,
            @PathVariable(value = "bookId") Long bookId
    ) throws ResourceNotFoundException {
        OrderDetailsKey orderDetailsKey = new OrderDetailsKey(orderId, bookId);

        return deleteOrderBookDetails(orderDetailsKey);
    }

    @DeleteMapping("/order-book-details/param")
    public ResponseEntity<Object> deleteOrderBookDetailsParam(
            @PathVariable(value = "orderId") Long orderId,
            @RequestParam(name = "bookId") Long bookId
    ) throws ResourceNotFoundException {
        OrderDetailsKey orderDetailsKey = new OrderDetailsKey(orderId, bookId);

        return deleteOrderBookDetails(orderDetailsKey);
    }

    public ResponseEntity<Object> deleteOrderBookDetails(OrderDetailsKey orderBookDetailsId) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";
        Boolean isPresent = false;
        OrderBookDetails orderBookDetailsEntity = new OrderBookDetails();

        List<OrderBookDetails> listAllOrderBookDetails = orderBookDetailsRepository.findAll();

        for (OrderBookDetails orderBookDetailsLoop: listAllOrderBookDetails) {
            if (orderBookDetailsLoop.getOrderDetailsId().equals(orderBookDetailsId)) {
                orderBookDetailsEntity = modelMapper.map(orderBookDetailsLoop, OrderBookDetails.class);
                isPresent = true;
            }
        }

        if (isPresent) {
            orderBookDetailsRepository.delete(orderBookDetailsEntity);
            countTotalOrder(orderRepository);

            message = "Order Book Details with orderId " + orderBookDetailsId.getOrderId() + " and bookId " + orderBookDetailsId.getBookId() + " was successfully deleted";
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Book Rating with Id " + orderBookDetailsId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    public BigDecimal countDiscount(Order order) {
        BigDecimal discount = new BigDecimal(0);
        if (order.getListOrderBookDetails().size() >= 3) {
            for (OrderBookDetails orderBookDetails: order.getListOrderBookDetails()) {
                discount = (BigDecimal.valueOf(orderBookDetails.getQuantity())
                        .multiply(orderBookDetails.getBook().getBookPrice())
                        .multiply(BigDecimal.valueOf(0.1)));
            }
        } else {
            for (OrderBookDetails orderBookDetails: order.getListOrderBookDetails()) {
                discount = BigDecimal.valueOf(0.0);
            }
        }

        return discount;
    }

    public void countTotalOrder(OrderRepository orderRepository) {
        List<Order> listAllOrder = orderRepository.findAll();

        for (Order order: listAllOrder) {
            BigDecimal totalOrder = new BigDecimal(0);
            BigDecimal totalDiscount = new BigDecimal(0);
            BigDecimal totalTax = new BigDecimal(0);
            BigDecimal totalPrice = new BigDecimal(0);

            if (!order.getListOrderBookDetails().isEmpty()) {
                for (OrderBookDetails orderBookDetails: order.getListOrderBookDetails()) {
                    totalDiscount = BigDecimal.valueOf(0); // (orderBookDetails.getDiscount());
                    totalTax = (orderBookDetails.getTax());
                    totalPrice = (BigDecimal.valueOf(orderBookDetails.getQuantity())
                            .multiply(orderBookDetails.getBook().getBookPrice()));

                    totalOrder = totalOrder.add(totalPrice.add(totalTax)).subtract(totalDiscount);
                }
            }
            order.setTotalOrder(totalOrder);
            orderRepository.save(order);
        }
    }
}
