package com.example.bookorderdiaz.controllers;

import com.example.bookorderdiaz.dtos.AuthorDto;
import com.example.bookorderdiaz.dtos.BookDto;
import com.example.bookorderdiaz.exceptions.ResourceNotFoundException;
import com.example.bookorderdiaz.models.Author;
import com.example.bookorderdiaz.models.Book;
import com.example.bookorderdiaz.models.Publisher;
import com.example.bookorderdiaz.repositories.AuthorRepository;
import com.example.bookorderdiaz.repositories.BookRepository;
import com.example.bookorderdiaz.repositories.PublisherRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class BookController {
    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private PublisherRepository publisherRepository;

    // Get all book
    @GetMapping("/books")
    public ResponseEntity<Object> getAllBooks() {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";
        List<Book> listAllBookEntity = bookRepository.findAll();
        List<BookDto> listAllBookDto = new ArrayList<>();

        if (listAllBookEntity.size() >= 1) {
            for (Book book: listAllBookEntity) {
                BookDto bookDto = modelMapper.map(book, BookDto.class);

                listAllBookDto.add(bookDto);
            }

            message = "read all books success";
            result.put("Data", listAllBookDto);
        } else {
            responseStatus = HttpStatus.NO_CONTENT;
            message = "Book list is empty";
        }

        result.put("Message", message);
        result.put("Total", listAllBookDto.size());
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Get a single book
    @GetMapping("/books/{bookId}")
    public ResponseEntity<Object> getBookByIdUrl(@PathVariable(value = "bookId") Long bookId) throws ResourceNotFoundException {
        return getBookById(bookId);
    }

    @GetMapping("/books/param")
    public ResponseEntity<Object> getBookByIdParam(@RequestParam(name = "bookId") Long bookId) throws ResourceNotFoundException {
        return getBookById(bookId);
    }

    public ResponseEntity<Object> getBookById(Long bookId) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (bookRepository.findById(bookId).isPresent()) {
            Book bookEntity = bookRepository.findById(bookId).get();
            BookDto bookDto = modelMapper.map(bookEntity, BookDto.class);

            message = "get Book success";
            result.put("Data", bookDto);
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Book with Id " + bookId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Post book
    @PostMapping("/books")
    public ResponseEntity<Object> createBook(@Valid @RequestBody Book book) {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (publisherRepository.findById(book.getPublisher().getPublisherId()).isPresent() && authorRepository.findById(book.getAuthor().getAuthorId()).isPresent()) {
            Publisher publisher = publisherRepository.findById(book.getPublisher().getPublisherId()).get();
            Author author = authorRepository.findById(book.getAuthor().getAuthorId()).get();

            book.setPublisher(publisher);
            book.setAuthor(author);
            book.calculateBookPrice();

            bookRepository.save(book);

            List<Book> listAllBooks = bookRepository.findAll();

            if (listAllBooks.contains(book)) {
                BookDto bookDto = modelMapper.map(book, BookDto.class);

                message = "create Book success";
                result.put("Data", bookDto);
            } else {
                responseStatus = HttpStatus.SERVICE_UNAVAILABLE;
                message = "create Book was failed";
            }
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Publisher or Author was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Update book
    @PutMapping("/books/{bookId}")
    public ResponseEntity<Object> updateBookUrl(
            @PathVariable(value = "bookId") Long bookId,
            @Valid @RequestBody Book bookDetails
    ) throws ResourceNotFoundException {
        return updateBook(bookId, bookDetails);
    }

    @PutMapping("/books/param")
    public ResponseEntity<Object> updateBookParam(
            @RequestParam(name = "bookId") Long bookId,
            @Valid @RequestBody Book bookDetails
    ) throws ResourceNotFoundException {
        return updateBook(bookId, bookDetails);
    }

    public ResponseEntity<Object> updateBook(Long bookId, Book bookDetails) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (bookRepository.findById(bookId).isPresent()) {
            bookDetails.setBookId(bookId);

            Book bookEntity = modelMapper.map(bookDetails, Book.class);

            if (publisherRepository.findById(bookEntity.getPublisher().getPublisherId()).isPresent() && authorRepository.findById(bookEntity.getAuthor().getAuthorId()).isPresent()) {
                Publisher publisher = publisherRepository.findById(bookEntity.getPublisher().getPublisherId()).get();
                Author author =  authorRepository.findById(bookEntity.getAuthor().getAuthorId()).get();

                bookEntity.setPublisher(publisher);
                bookEntity.setAuthor(author);
                bookEntity.calculateBookPrice();

                Book updatedBookEntity = bookRepository.save(bookEntity);

                List<Book> listAllBooks = bookRepository.findAll();

                if (listAllBooks.contains(updatedBookEntity)) {
                    AuthorDto authorDto = modelMapper.map(bookEntity.getAuthor(), AuthorDto.class);

                    BookDto updatedBookDto = modelMapper.map(updatedBookEntity, BookDto.class);

                    updatedBookDto.setAuthor(authorDto);

                    message = "update Book success";
                    result.put("Data", updatedBookDto);
                } else {
                    responseStatus = HttpStatus.SERVICE_UNAVAILABLE;
                    message = "update Book was failed";
                }
            } else {
                responseStatus = HttpStatus.NOT_FOUND;
                message = "Publisher or Author was not found";
            }
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Book with Id " + bookId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

    // Delete book
    @DeleteMapping("/books/{bookId}")
    public ResponseEntity<Object> deleteBookUrl(@PathVariable(value = "bookId") Long bookId) throws ResourceNotFoundException {
        return deleteBook(bookId);
    }

    @DeleteMapping("/books/param")
    public ResponseEntity<Object> deleteBookParam(@RequestParam(name = "bookId") Long bookId) throws ResourceNotFoundException {
        return deleteBook(bookId);
    }

    public ResponseEntity<Object> deleteBook(Long bookId) throws ResourceNotFoundException {
        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        if (bookRepository.findById(bookId).isPresent()) {
            Book bookEntity = bookRepository.findById(bookId).get();

            bookRepository.delete(bookEntity);

            message = "Book with id " + bookEntity.getBookId() + " was successfully deleted";
        } else {
            responseStatus = HttpStatus.NOT_FOUND;
            message = "Book with Id " + bookId + " was not found";
        }

        result.put("Message", message);
        result.put("Status", responseStatus.value());

        return new ResponseEntity<>(result, responseStatus);
    }

}
