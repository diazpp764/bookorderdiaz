package com.example.bookorderdiaz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class BookorderdiazApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookorderdiazApplication.class, args);
	}

}
