package com.example.bookorderdiaz.repositories;

import com.example.bookorderdiaz.models.BookRating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRatingRepository extends JpaRepository<BookRating, Long> {
}
