package com.example.bookorderdiaz.repositories;

import com.example.bookorderdiaz.models.Paper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaperRepository extends JpaRepository<Paper, Long> {
}
