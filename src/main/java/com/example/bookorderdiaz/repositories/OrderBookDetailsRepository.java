package com.example.bookorderdiaz.repositories;

import com.example.bookorderdiaz.models.OrderBookDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderBookDetailsRepository extends JpaRepository<OrderBookDetails, Long> {
}
